export YO=$(jq .api_definition.use_keyless ./api-*.json)
echo "$YO"
if [ $YO = "false" ]
then 
    echo "$YO USE KEYLESS IS FALSE"
    exit 1
else
    echo "USE KEYLESS IS TRUE"
    exit 0
fi